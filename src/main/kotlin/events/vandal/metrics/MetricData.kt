package events.vandal.metrics

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.mojang.authlib.GameProfile
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService
import java.net.Proxy
import java.util.*

abstract class MetricData(val profile: GameProfile) {
    open fun serialize(): JsonObject {
        return JsonObject().apply {
            addProperty("name", profile.name)
            addProperty("uuid", profile.id.toString())
        }
    }

    companion object {
        private val sessionService = YggdrasilAuthenticationService(Proxy.NO_PROXY, null)
            .createMinecraftSessionService()

        private val profileCache = mutableMapOf<UUID, GameProfile>()

        fun deserialize(data: JsonObject): GameProfile {
            val uuid = UUID.fromString(data.get("uuid").asString)
            if (profileCache[uuid] != null)
                return profileCache[uuid]!!

            val profileProperties = sessionService.fillProfileProperties(GameProfile(uuid, data.get("name").asString), true)
            profileCache[uuid] = profileProperties

            return profileProperties
        }

        // this should exist in Gson
        fun getOrNull(data: JsonObject, key: String): JsonElement? {
            return if (data.has(key)) data.get(key) else null
        }
    }
}
